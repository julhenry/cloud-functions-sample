import * as admin from "firebase-admin";
import * as functions from "firebase-functions";
import {firestore} from "firebase-admin";
admin.initializeApp();

const COLLECTION_PATH: string = "books";

export const addBook = functions.https.onRequest((req: any, res: any) => {
    const {name} = req.body;
    const {author} = req.body;
    const {createdAt} = req.body;

    if (req.method !== "POST") {
        return res.status(403).send("Unauthorized");
    } else if (name === undefined) {
        return res.status(400).send("Invalid name");
    } else if (author === undefined) {
        return res.status(400).send("Invalid author");
    } else if (createdAt === undefined) {
        return res.status(400).send("Invalid datePublished");
    }

    const id = firestore().collection(COLLECTION_PATH).doc().id;

    return firestore().collection(COLLECTION_PATH).doc(id).set({
        id: id,
        name: name,
        author: author,
        datePublished: createdAt,
    })
        .then(() => res.status(201).send("Book created"))
        .catch((error) => res.status(400).send(error));
});

export const updateBook = functions.https.onRequest((req: any, res: any) => {
    const {id} = req.body;
    const {name} = req.body;
    const {author} = req.body;
    const {datePublished} = req.body;

    if (req.method !== "POST") {
        return res.status(403).send("Unauthorized");
    }

    return firestore().collection(COLLECTION_PATH).doc(id).update({
        name: name ?? "",
        author: author ?? "",
        datePublished: datePublished ?? "",
    })
        .then(() => res.status(201).send("Book updated"))
        .catch((error) => res.status(400).send(error));
});


export const deleteBook = functions.https.onRequest((req: any, res: any) => {
    const {id} = req.body;

    if (req.method !== "DELETE") {
        return res.status(403).send("Unauthorized");
    } else if (id === undefined) {
        return res.status(400).send("Invalid id");
    }

    return firestore().collection(COLLECTION_PATH).doc(id).delete()
        .then(() => res.status(201).send("Book deleted"))
        .catch((error) => res.status(400).send(error));
});
